/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pert_cpm_project;

import java.util.ArrayList;

/**
 *
 * @author 1140921
 */
public class Activity implements Comparable<Activity> {

    /**
     * chave de identificação
     */
    private String activKey;

    /**
     * tipo de atividade
     */
    private String activType;

    /**
     * descrição da atividade
     */
    private int delayTime;

    /**
     * duração da atividade
     */
    private float duration;
    
    private String activityID;
    

    /**
     *
     * @param activityKey
     * @param activType
     * @param activityID
     * @param durationtime
     *
     */
    public Activity(String activityKey,String activityID, String activType, int durationtime, int delayTime) {
        setActivKey(activityKey);
        setActivityID(activityID);
        setActivType(activType);
        setDelayTime(delayTime);
        setDuration(durationtime);

    }

    /**
     * @return the activKey
     */
    public String getActivKey() {
        return activKey;
    }

    /**
     * @param activKey the activKey to set
     */
    public final void setActivKey(String activKey) {
        this.activKey = activKey;
    }

    /**
     * @return the activType
     */
    public String getActivType() {
        return activType;
    }

    /**
     * @param activType the activType to set
     */
    public final void setActivType(String activType) {
        this.activType = activType;
    }

    /**
     * @return the activityID
     */
    public int getDelayTime() {
        return delayTime;
    }

    /**
     * @param delayTime the activityID to set
     */
    public final void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

    /**
     * @return the duration
     */
    public float getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public final void setDuration(float duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {

        return "Key :  " + this.activKey + "\nActivity Key : " 
                + this.activityID;
//                + "\nDescription : "
//                + this.delayTime + "\nDuration : " + this.duration;
    }

    @Override
    public int compareTo(Activity other) {

        if (this.delayTime < other.getDelayTime()) {
            return -1;
        }
        if (this.delayTime > other.getDelayTime()) {
            return 1;
        }
        int i = this.activKey.compareTo(other.getActivKey());
        if (i != 0) {
            return i;
        }
        return this.activType.compareTo(other.getActivType());
    }

    /**
     * @return the activityID
     */
    public String getActivityID() {
        return activityID;
    }

    /**
     * @param description the activityID to set
     */
    public void setActivityID(String description) {
        this.activityID = description;
    }

}
