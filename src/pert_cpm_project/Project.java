/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pert_cpm_project;

/**
 *
 * @author 1140921
 */
public class Project implements Comparable<Project> {

    private String projetoID;

    private String description;

    private int completionTime;

    private int delayTime;

   // private ActivityList activityList;

    public Project(String projectId, String description, int completionTime, int delayTime) {
        setProjetoID(projectId);
        setDescription(description);
        setCompletionTime(completionTime);
        setDelayTime(delayTime);

    }

    /**
     * @return the projetoID
     */
    public String getProjetoID() {
        return projetoID;
    }

    /**
     * @param projetoID the projetoID to set
     */
    public void setProjetoID(String projetoID) {
        this.projetoID = projetoID;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the completionTime
     */
    public int getCompletionTime() {
        return completionTime;
    }

    /**
     * @param completionTime the completionTime to set
     */
    public void setCompletionTime(int completionTime) {
        this.completionTime = completionTime;
    }

    /**
     * @return the delayTime
     */
    public int getDelayTime() {
        return delayTime;
    }

    /**
     * @param delayTime the delayTime to set
     */
    public void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

//    /**
//     * @return the activityList
//     */
//    public ActivityList getActivityList() {
//        return activityList;
//    }
//
//    /**
//     * @param activityList the activityList to set
//     */
//    public void setActivityList(ActivityList activityList) {
//        this.activityList = activityList;
//    }

    public String toString() {
        return "Project ID :" + getProjetoID() ;
//                + "\nDescription :" + getDescription() + "\nCompletion Time :" + getCompletionTime() + "\nDetlay Time :" + getDelayTime();

    }

//    public String stringBuilder() {
//        String stringBuilder = " ";
//        for (String lst : listPrecedence) {
//            stringBuilder = lst.toString() + "," + stringBuilder;
//
//        }
//        return stringBuilder;
//    }
    @Override
    public int compareTo(Project otherProject) {

        if (this.delayTime < otherProject.delayTime) {
            return -1;
        }
        if (this.delayTime > otherProject.delayTime) {
            return 1;
        }
        return this.projetoID.compareTo(otherProject.getProjetoID());
    }

}
