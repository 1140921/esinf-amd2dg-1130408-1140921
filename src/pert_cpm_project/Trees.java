/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pert_cpm_project;

import Tree.BST;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author 1140921
 */
public class Trees {

    /**
     * BST that contains all the projects.
     *
     *
     */
    private BST<Project> projectsTree;
    /**
     * BST that contains all the activities of the projects.
     */
    private BST<Activity> activitiesTree;

  

    /**
     * Empty constructor.
     */
    public Trees() {
        projectsTree = new BST<>();
        activitiesTree = new BST<>();
    }

    public BST<Project> getProjectsTree() {
        return projectsTree;
    }

    public BST<Activity> getActivitiesTree() {
        return activitiesTree;
    }

    public void setProjectsTree(BST<Project> pBST) {
        projectsTree = pBST;
    }

    public void setActivitiesTree(BST<Activity> aBST) {
        activitiesTree = aBST;
    }

    /**
     * Method that inserts a project in the right tree.
     *
     * @param project project to be added
     */
    public void insert(Project project) {
        projectsTree.insert(project);
    }

    /**
     * Method that inserts an activity in the right tree.
     *
     * @param activity activity to be added
     */
    public void insert(Activity activity) {
        activitiesTree.insert(activity);
    }

    /**
     * Method that searches for a project, in the BST, by the reference and
     * returns it. Returns null if there isn't a project with the same
     * reference.
     *
     * @param ref reference to search in the project BST
     * @return project if found, null otherwise
     */
    public Project getProjectByID(String ref) {
        for (Project p : projectsTree.inOrder()) {
            if (p.getProjetoID().equalsIgnoreCase(ref)) {
                return p;
            }
        }
        return null;
    }
    public Activity getActivityByID(String ref) {
        for (Activity a : activitiesTree.inOrder()) {
            if (a.getActivityID().equalsIgnoreCase(ref)) {
                return a;
            }
        }
        return null;
    }

    /**
     * Method that verifies if a project is contained in the project tree.
     *
     * @param project project to verify
     * @return true if found, false if not found
     */
    public boolean contains(Project project) {
        for (Project p : projectsTree.inOrder()) {
            if (p.equals(project)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method that verifies if an activity is contained in the activity tree.
     *
     * @param activity
     * @return
     */
    public boolean contains(Activity activity) {
        for (Activity a : activitiesTree.inOrder()) {
            if (a.equals(activity)) {
                return true;
            }
        }
        return false;
    }

    /**
     * code activity, type activity, duration time, delay time).
     *
     * @return
     */
    public String printProjectsByDelay() {
        String str = "";
        for (Project p : projectsTree.inOrder()) {
            str += "\nProject: Reference:   " + p.getProjetoID() + ";   "
                    + "Delay time:   " + p.getDelayTime() + ";";
            for (Activity a : activitiesTree.inOrder()) {
                if (a.getActivKey().equalsIgnoreCase(p.getProjetoID())) {
                    str += "\n\tActivity:   " + a.getActivityID() + ";   "
                            + "Type:   " + a.getActivType() + ";   "
                            + "Duration Time:   " + a.getDuration() + ";   "
                            + "Delay Time:   " + a.getDelayTime() + ";";
                }
            }
        }
        return str;
    }

    public Map lateActivitiesSameType(Project p1, Project p2,String type) {

      

        if (p1 == null || p2 == null) {
            return null;
        }
        if (p1.getDelayTime() == 0 || p2.getDelayTime() == 0) {
            return null;
        }

        Map<String, List<Activity>> map = new HashMap<>();

        map = searchTypeActivity(p1, p2, type);

        return map;
    }

    private Map<String, List<Activity>> searchTypeActivity(Project p1, Project p2, String type) {
        Map<String, List<Activity>>  late_activities =  new HashMap<>();
        List<Activity> la1 = new ArrayList<>();
        String ref;
        for (Activity a : this.activitiesTree.inOrder()) {
            if (a.getDelayTime() != 0 && a.getActivType().equalsIgnoreCase(type)) {
                ref = a.getActivKey();
                if (ref.equalsIgnoreCase(p1.getProjetoID())) {
                    la1.add(a);
                } else if (ref.equalsIgnoreCase(p2.getProjetoID())) {
                    la1.add(a);
                }
            }
        }
        late_activities.put(type,la1);
        return late_activities;
    }

}
