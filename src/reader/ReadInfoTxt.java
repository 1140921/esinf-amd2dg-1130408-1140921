package reader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import pert_cpm_project.Activity;
import pert_cpm_project.Project;
import pert_cpm_project.Trees;

/**
 *
 * @author 1140921
 */
public class ReadInfoTxt {

    public static void readInfoTxt(String FILE_TXT, Trees projectsTree,Trees activityTree) throws FileNotFoundException {
        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");
        int projectsNumber = Integer.parseInt(fInput.nextLine());

        for (int i = 0; i < projectsNumber; i++) {
            String linha = fInput.nextLine();
            // Verifica se linha não está em branco
            if (linha.length() > 0) {
                saveProjectsTree(linha, projectsTree);
            }

        }
        int activityNumber = Integer.parseInt(fInput.nextLine());
        for (int i = 0; i < activityNumber; i++) {
            String linha = fInput.nextLine();
            // Verifica se linha não está em branco
            if (linha.length() > 0) {
                saveActivitiesProjectsTree(linha, projectsTree,activityTree);
            }
        }
        fInput.close();

    }

    private static void saveProjectsTree(String linha, Trees projectsTree) {
        // separador de dados por linha
        String[] temp = linha.split(",");
        if (temp.length == 4) {
            String projectID = temp[0];
            String description = temp[1];
            int completionTime = Integer.parseInt(temp[2]);
            int delayTime = Integer.parseInt(temp[3]);
            Project p = new Project(projectID, description, completionTime, delayTime);
            projectsTree.insert(p);

        }
    }

    private static void saveActivitiesProjectsTree(String linha, Trees projectTree, Trees activityTree) {

        String[] temp = linha.split(",");
        if (temp.length == 5) {
            String projectID = temp[0];
            String activityID = temp[1];
            String activType = temp[2];
            int durationTime = Integer.parseInt(temp[3]);
            int delayTime = Integer.parseInt(temp[4]);
            if (projectTree.getProjectByID(projectID) != null) {
                
                Activity a = new Activity(projectID, activityID, activType, durationTime, delayTime);
                activityTree.insert(a);
            }

        }

    }
}
