/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pert_cpm_project;

import Tree.BST;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import reader.ReadInfoTxt;

/**
 *
 * @author 1140921
 */
public class TreesTest {

    public TreesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    String FILE_TXT = "Part3TestFile.txt";

 
//
    /**
     * Test of printProjectsByDelay method, of class Trees.
     */
    @Test
    public void testPrintProjectsByDelay() throws FileNotFoundException {
        System.out.println("printProjectsByDelay");
         Trees activityTree = new Trees();
        Trees projectTree = new Trees();
        ReadInfoTxt.readInfoTxt(FILE_TXT, projectTree,activityTree);
        projectTree.setActivitiesTree(activityTree.getActivitiesTree());
        System.out.println(projectTree.printProjectsByDelay());
        
    }



    /**
     * Test of lateActivitiesSameType method, of class Trees.
     */
    @Test
    public void testLateActivitiesSameType() throws FileNotFoundException {
        System.out.println("lateActivitiesSameType");
         Trees activityTree = new Trees();
        Trees projectTree = new Trees();
        ReadInfoTxt.readInfoTxt(FILE_TXT, projectTree,activityTree);
        projectTree.setActivitiesTree(activityTree.getActivitiesTree());
        Project p1 = projectTree.getProjectByID("p1");
        Project p2 = projectTree.getProjectByID("p4");
        
        Map result = projectTree.lateActivitiesSameType(p1, p2, "VCA");
     
        System.out.println(result.toString());
    }

}
