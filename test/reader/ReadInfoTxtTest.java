/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pert_cpm_project.Trees;

/**
 *
 * @author 1140921
 */
public class ReadInfoTxtTest {

    public ReadInfoTxtTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of readInfoTxt method, of class ReadInfoTxt.
     */
    @Test
    public void testReadInfoTxt() throws Exception {
        System.out.println("readInfoTxt");
        String FILE_TXT = "Part3TestFile.txt";
        Trees activityTree = new Trees();
        Trees projectTree = new Trees();
        ReadInfoTxt.readInfoTxt(FILE_TXT, projectTree,activityTree);
        System.out.println("Project Tree");
        System.out.println("-->"+projectTree.getProjectsTree());
        System.out.println("Activity Tree");
        System.out.println("-"+activityTree.getActivitiesTree());
        
        
    }

}
